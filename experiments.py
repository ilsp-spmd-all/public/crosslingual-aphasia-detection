import argparse
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import LeaveOneOut
from xgboost import XGBClassifier
from sklearn.svm import SVC
from sklearn import tree
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from utils import load_dict, save_dict
from feature_names import FEATURES_NAMES


def parse_arguments():
    parser = argparse.ArgumentParser(description="Experiments Argument Parser")
    parser.add_argument(
        "--data-dict-path-source",
        type=str,
        help="Data dict path source",
    )
    parser.add_argument(
        "--data-dict-path-target",
        type=str,
        help="Data dict path target",
    )
    parser.add_argument(
        "--model",
        type=str,
        default="xgboost",
        help="Classification model",
    )
    parser.add_argument(
        "--mono-english",
        action="store_true",
        help="Monolingual Experiment",
    )
    parser.add_argument(
        "--zero-shot",
        action="store_true",
        help="Monolingual Experiment",
    )
    return parser.parse_args()


def get_dataset(data_dict):
    X, y = [], []
    for speaker in data_dict.keys():
        group = data_dict[speaker]["user_info"]["group"]
        if group == "aphasia":
            label = 1
        elif group in ["control", "controls", "cardiacs"]:
            label = 0
        else:
            exit("Problem with group name - label")
        y.append(label)
        features = list(data_dict[speaker]["features"].values())
        X.append(features)
    return np.array(X), np.array(y)


def zeroshot_experiment(train_dict, test_dict, model="xgboost"):
    X_train, y_train = get_dataset(train_dict)
    X_test, y_test = get_dataset(test_dict)

    nan_ids_from_X_train = np.any(np.isnan(X_train), axis=0)

    X_train = X_train[:, ~nan_ids_from_X_train]
    nan_ids_from_X_test = np.any(np.isnan(X_test), axis=0)
    X_test = X_test[:, ~nan_ids_from_X_test]
    print(X_train.shape, X_test.shape)

    english_scaler = StandardScaler()
    X_train = english_scaler.fit_transform(X_train)

    if model == "xgboost":
        xgb_classifier = XGBClassifier()
        xgb_classifier.fit(X_train, y_train)

        X_test = english_scaler.transform(X_test)
        y_preds = xgb_classifier.predict(X_test)
        acc = xgb_classifier.score(X_test, y_test)
        weights = xgb_classifier.get_booster().get_score(importance_type="weight")
        for idx, weight_key in enumerate(weights):
            print(
                f"Feature {FEATURES_NAMES[idx].ljust(30)} \t weight={weights[weight_key]}"
            )
    elif model == "svm":
        clf = SVC(gamma="auto", kernel="linear")
        clf.fit(X_train, y_train)
        X_test = english_scaler.transform(X_test)
        y_preds = clf.predict(X_test)
        acc = clf.score(X_test, y_test)
    elif model == "tree":
        clf = tree.DecisionTreeClassifier()
        clf.fit(X_train, y_train)
        X_test = english_scaler.transform(X_test)
        y_preds = clf.predict(X_test)
        acc = clf.score(X_test, y_test)
    elif model == "forest":
        clf = RandomForestClassifier()
        clf.fit(X_train, y_train)
        X_test = english_scaler.transform(X_test)
        y_preds = clf.predict(X_test)
        acc = clf.score(X_test, y_test)
    print(acc)


def loso_english_experiment(data_dict):
    X, y = get_dataset(data_dict)

    kf = LeaveOneOut()
    nan_ids_from_X = np.any(np.isnan(X), axis=0)
    X = X[:, ~nan_ids_from_X]
    kf_split = kf.split(X=X)

    for model_name in ["xgboost", "svm", "tree", "forest"]:
        accuracy_list = []
        f1_list = []
        tmp_kf_split = kf_split
        for train, test in kf.split(X=X):
            X_train, y_train = X[train], y[train]
            X_test, y_test = X[test], y[test]
            if model_name == "svm":
                clf = make_pipeline(
                    StandardScaler(), SVC(gamma="auto", kernel="linear")
                )
            elif model_name == "xgboost":
                clf = make_pipeline(StandardScaler(), XGBClassifier(eta=0.2))
            elif model_name == "tree":
                clf = make_pipeline(StandardScaler(), tree.DecisionTreeClassifier())
            elif model_name == "forest":
                clf = make_pipeline(StandardScaler(), RandomForestClassifier())
            clf.fit(X_train, y_train)
            y_preds = clf.predict(X_test)
            acc = clf.score(X_test, y_test)
            accuracy_list.append(acc)
        accuracy = sum(accuracy_list) / len(accuracy_list)
        print(f"Model: {model_name} - Accuracy: {accuracy}")
    return accuracy


if __name__ == "__main__":
    args = parse_arguments()
    if args.mono_english:
        loso_english_experiment(data_dict=load_dict(args.data_dict_path_source))
    elif args.zero_shot:
        zeroshot_experiment(
            train_dict=load_dict(args.data_dict_path_source),
            test_dict=load_dict(args.data_dict_path_target),
            model=args.model,
        )
    else:
        print("Select an experiment. Add --mono-english or --zero-shot")
